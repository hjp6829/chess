using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SaveLoadUI : UI
{
    [SerializeField] private GameLoadButton saveloadButton;
    [SerializeField] private Transform viewContent;
    [SerializeField] private TextMeshProUGUI saveNameTEXT;
    [SerializeField] private OverWriteUI overWriteUI;
      [SerializeField] private Image previewImage;
    private ChessDatamanager datamanager;
    private List<GameObject> nuttonList = new List<GameObject>();
    public override void Open()
    {
        base.Open();
        datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        UpdateList();
    }
    private void UpdateList()
    {
        for(int i=0;i<nuttonList.Count;i++)
        {
            Destroy(nuttonList[i]);
        }
        nuttonList.Clear();
        List<SaveData> list = datamanager.SaveList.list;
        if (list == null)
            return;
        foreach (SaveData data in list)
        {
            GameLoadButton temp = Instantiate(saveloadButton);
            temp.SetGameData(data);
            temp.GetComponent<Button>().onClick.AddListener(() =>
            {
                previewImage.sprite = datamanager.LoadSaveImage(data.saveName);
            });
            temp.transform.SetParent(viewContent);
            nuttonList.Add(temp.gameObject);
        }
    }
    public void SaveGameData()
    {
        if (string.IsNullOrWhiteSpace(saveNameTEXT.text))
        {
            Debug.Log("�۵�");
            return;
        }
        if (datamanager.CheckOverWrite(saveNameTEXT.text))
        { 
            overWriteUI.Open();
            overWriteUI.OnAccept += () =>
            {
                datamanager.AddNewSave(saveNameTEXT.text);
                UpdateList();
            };
            return;
        }
        datamanager.AddNewSave(saveNameTEXT.text);
        UpdateList();
    }
}
