using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MultiRoomList : UI
{
    [SerializeField] private GameObject roomButton;
    [SerializeField] private Transform content;
    [SerializeField] private TextMeshProUGUI roomName;
    [SerializeField] private TMP_InputField chatWritePlace;
    private Dictionary<string, GameObject> roomButtonDic=new Dictionary<string, GameObject>();
    private void Start()
    {
        Networkmanager netmanager = ServiceLocatorManager.GetService<Networkmanager>();
        netmanager.OnJoinRoom += () =>
        {
            Close();
        };
    }
    public void CreateRoomList(RoomInfo roominfo)
    {
        if (roomButtonDic.ContainsKey(roominfo.Name))
            return;
        GameObject temp = Instantiate(roomButton);
        Networkmanager netmanager = ServiceLocatorManager.GetService<Networkmanager>();
        temp.GetComponent<Button>().onClick.AddListener(() =>
        {
            PhotonNetwork.JoinRoom(roominfo.Name);
            UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
            uimanager.OpenUI(UItype.CONNECTING);
        });
        temp.GetComponent<JoinRoomButton>().SetButtonName(roominfo.Name);
        temp.transform.SetParent(content);
        roomButtonDic.Add(roominfo.Name, temp);
    }
    public void DeleteRoomList(RoomInfo info)
    {
        if (!roomButtonDic.ContainsKey(info.Name))
            return;
        GameObject temp= roomButtonDic[info.Name];
        Destroy(temp);
        roomButtonDic.Remove(info.Name);    
    }
    public void OnCreateRoom()
    {
        Networkmanager netmanager = ServiceLocatorManager.GetService<Networkmanager>();
        UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
        uimanager.OpenUI(UItype.CONNECTING);
        netmanager.CreateNewRoom(roomName.text);
        chatWritePlace.text = "";
    }
}
