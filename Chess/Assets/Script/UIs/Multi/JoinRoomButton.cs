using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class JoinRoomButton : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI buttonName;

    public void SetButtonName(string buttonname)
    {
        buttonName.text = buttonname;
    }
}
