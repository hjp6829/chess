using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomPlayerUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI playerName;
    private Image bg;
    private Photon.Realtime.Player playerData;

    public Photon.Realtime.Player PlayerData { get => playerData; }
    private void Start()
    {
        bg = GetComponent<Image>();
        playerName.text = "";
    }
    public void SetPlayer(Photon.Realtime.Player playerData)
    {
        this.playerData=playerData;
        playerName.text = playerData.NickName;
    }
    public void removePlayerData()
    {
        this.playerData = null;
        playerName.text = "";
        bg.color = Color.white;
    }
    public void SetReadyColor()
    {
        bg.color = Color.blue;
    }
    public void SetResetColor()
    {
        bg.color = Color.white;
    }
}
