using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultiGameStart : UI
{
    private int selectCount;
    [SerializeField] private Button GamestartButton;
    [SerializeField] private Button SelectWhiteButton;
    [SerializeField] private Button SelectBlackButton;
    [SerializeField] private GameObject SelectColorUI;
    [SerializeField] private GameObject Bg;
    private PhotonView photonview;
    private Dictionary<string,bool> playerStartDic = new Dictionary<string,bool>();
    [SerializeField]
    private List<RoomPlayerUI> roomPlayers = new List<RoomPlayerUI>();
    private bool isGameStart;
    [SerializeField] private GameObject leaveRoomButton;
    public override void initialze()
    {
        photonview=GetComponent<PhotonView>();
        Networkmanager networkmanager = ServiceLocatorManager.GetService<Networkmanager>();
        networkmanager.OnJoinNewPlayer += (Photon.Realtime.Player newplayer) =>
        {
            playerStartDic.Add(newplayer.NickName, false);
            foreach(var playerUI in roomPlayers)
            {
               if(playerUI.PlayerData==null)
                {
                    playerUI.SetPlayer(newplayer);
                }
            }
        };
        networkmanager.OnLeftNewPlayer += (Photon.Realtime.Player newplayer) =>
        {
            playerStartDic.Remove(newplayer.NickName);
            RoomPlayerUI temp = roomPlayers.Find(x=>x.PlayerData==newplayer);
            playerStartDic[PhotonNetwork.LocalPlayer.NickName] = false;
            roomPlayers.Find(x => x.PlayerData == PhotonNetwork.LocalPlayer).SetResetColor();
            selectCount = 0;
            temp.removePlayerData();
            photonview.RPC("SendCloseMessage", RpcTarget.All);
            if (isGameStart)
            {
                isGameStart = false;
                Gamemanager gamemanager = ServiceLocatorManager.GetService<Gamemanager>();
                gamemanager.OnMultiGameReset?.Invoke();
                Open();
            }
        };
        networkmanager.OnJoinRoom += () =>
        {
            playerStartDic.Clear();
            Debug.Log("룸에 입장");
            int i = 0;
            foreach (var player in PhotonNetwork.PlayerList)
            {
                //if (player != PhotonNetwork.LocalPlayer)
                playerStartDic.Add(player.NickName, false);
                roomPlayers[i].SetPlayer(player);
                i++;
            }
        };
        networkmanager.OnLeftPlayer += () =>
        {
            selectCount = 0;
            photonview.RPC("SendCloseMessage", RpcTarget.All);
        };
        SelectWhiteButton.onClick.AddListener(() =>
        {
            Gamemanager gamemanager = ServiceLocatorManager.GetService<Gamemanager>();
            gamemanager.OwnerColor = ChessColor.WHITE;
            photonview.RPC("WhiteButtonInteractable", RpcTarget.All);
            SelectBlackButton.interactable = false;
        });
        SelectBlackButton.onClick.AddListener(() =>
        {
            Gamemanager gamemanager = ServiceLocatorManager.GetService<Gamemanager>();
            gamemanager.OwnerColor = ChessColor.BLACK;
            photonview.RPC("BlackButtonInteractable", RpcTarget.All);
            SelectWhiteButton.interactable = false;
        });
    }
    public override void Open()
    {
        base.Open();
        leaveRoomButton.SetActive(false);
    }
    
    public void GameReadyButton()
    {
        Networkmanager newmanager = ServiceLocatorManager.GetService<Networkmanager>();
        if (!newmanager.PlayerCountChecker())
            return;
        photonview.RPC("NotifyButtonClicked", RpcTarget.Others, PhotonNetwork.LocalPlayer.NickName);
        photonview.RPC("ChangePlayerStartColor", RpcTarget.All, PhotonNetwork.LocalPlayer.NickName);
        bool isOtherCheck = true;
        playerStartDic[PhotonNetwork.LocalPlayer.NickName] = true;
        foreach (var other in playerStartDic)
        {
            Debug.Log("시작" + other.Value);
            if (!other.Value)
            {
                isOtherCheck = false;
                break;
            }
        }
        Debug.Log("시작"+ isOtherCheck);
        if (!isOtherCheck)
            return;
        photonview.RPC("MultiGameStat", RpcTarget.All);

    }
    [PunRPC]
    private void NotifyButtonClicked(string username)
    {
        playerStartDic[username] = true;
    }
    [PunRPC]
    private void ChangePlayerStartColor(string username)
    {
        RoomPlayerUI temp = roomPlayers.Find(x => x.PlayerData.NickName == username);
        temp.SetReadyColor();
    }
    [PunRPC]
    private void MultiGameStat()
    {
        GamestartButton.interactable = false;
        SelectColorUI.SetActive(true);
    }
    [PunRPC]
    public void WhiteButtonInteractable()
    {
        SelectWhiteButton.interactable = false;
        selectCount++;
        if (selectCount != 2)
            return;
        isGameStart = true;
        UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
        uimanager.ResetUIStack();
        Bg.SetActive(false);
        photonview.RPC("SendCloseMessage", RpcTarget.All);
        ServiceLocatorManager.GetService<Gamemanager>().OnMultiGameStart?.Invoke();
        leaveRoomButton.SetActive(true);
    }
    [PunRPC]
    public void BlackButtonInteractable()
    {
        SelectBlackButton.interactable = false;
        selectCount++;
        if (selectCount != 2)
            return;
        isGameStart = true;
        UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
        uimanager.ResetUIStack();
        Bg.SetActive(false);
        photonview.RPC("SendCloseMessage", RpcTarget.All);
        ServiceLocatorManager.GetService<Gamemanager>().OnMultiGameStart?.Invoke();
        leaveRoomButton.SetActive(true);
    }
    public override void Close()
    {
        base.Close();
        Networkmanager newmanager = ServiceLocatorManager.GetService<Networkmanager>();
        photonview.RPC("SendCloseMessage", RpcTarget.All);
        newmanager.LeftRoom();
        foreach(var playerReadyImage in roomPlayers)
        {
            playerReadyImage.removePlayerData();
        }

    }
    [PunRPC]
    public void SendCloseMessage()
    {
        GamestartButton.interactable = true;
        SelectWhiteButton.interactable = true;
        SelectBlackButton.interactable = true;
        SelectColorUI.SetActive(false);
    }
}
