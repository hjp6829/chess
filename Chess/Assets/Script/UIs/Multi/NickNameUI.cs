using Photon.Pun.Demo.Cockpit;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NickNameUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI nowUserName;
    [SerializeField] private TextMeshProUGUI newUserName;
    [SerializeField] private TMP_InputField nickNameField;
    private void Start()
    {
        SetNowUserName();
    }
    public void SetUserNickName()
    {
        Networkmanager newmanager = ServiceLocatorManager.GetService<Networkmanager>();
        newmanager.SetNewUserName(newUserName.text);
        nickNameField.text = "";
        SetNowUserName();
    }
    public void SetNowUserName()
    {
        Networkmanager newmanager = ServiceLocatorManager.GetService<Networkmanager>();
        nowUserName.text = newmanager.UserName;
    }
}
