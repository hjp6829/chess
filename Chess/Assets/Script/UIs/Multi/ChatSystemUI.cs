using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ChatSystemUI : UI
{
    [SerializeField] private TextMeshProUGUI chatTEXT;
    [SerializeField] private TMP_InputField chatWritePlace;
    [SerializeField] private Transform content;
    [SerializeField] private ScrollRect scrollRect;
    private PhotonView photonview;
    private string chatData;
    private string userName;
    private void Start()
    {
        chatTEXT.text = "";
        photonview = GetComponent<PhotonView>();
        Networkmanager networkmanager = ServiceLocatorManager.GetService<Networkmanager>();
        userName = networkmanager.UserName;
        networkmanager.OnJoinNewPlayer += (Photon.Realtime.Player user) =>
        {
            photonview.RPC("RPC_Chat", RpcTarget.All, "NewPlayer Entered Room : "+user.NickName);
        };
    }
    private void AddChatMessage(string message)
    {
        //TextMeshProUGUI textTemp = PhotonView.Instantiate(chatTEXT);
        //textTemp.text = message;
        //textTemp.transform.SetParent(content);
        chatData += message;
        chatTEXT.text = chatData;
        Canvas.ForceUpdateCanvases();
        scrollRect.verticalNormalizedPosition = 0;
    }
    public void OnEndEditEvent(string message)
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            chatWritePlace.text = "";
            string strMessage = userName + " : " + message+ System.Environment.NewLine;
            photonview.RPC("RPC_Chat", RpcTarget.Others, strMessage);
            AddChatMessage(strMessage);
            EventSystem.current.SetSelectedGameObject(chatWritePlace.gameObject, null);
        }
    }
    [PunRPC]
    private void RPC_Chat(string message)
    {
        AddChatMessage(message);
    }
}
