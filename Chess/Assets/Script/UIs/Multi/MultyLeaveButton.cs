using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultyLeaveButton : MonoBehaviour
{
    public void LeaveMultyGame()
    {
        Gamemanager gamemanager = ServiceLocatorManager.GetService<Gamemanager>();
        gamemanager.OnMultiGameLeave?.Invoke();
        Loadingmanager.LoadScene("MenuSene");
    }
}
