using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectColorUI : UI
{
    public void GameStart(int idx)
    {
        Gamemanager gamemanager = ServiceLocatorManager.GetService<Gamemanager>();
        Loadingmanager.LoadScene("GameScene", () =>
        {
            gamemanager.OnGameStart?.Invoke();
        });
        Close();
    }
}
