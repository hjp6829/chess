using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameEndUI : UI
{
    [SerializeField] private TextMeshProUGUI endTEXT;

    public override void Open()
    {
        base.Open();
        Gamemanager gamemanger = ServiceLocatorManager.GetService<Gamemanager>();
        if (gamemanger.WinColor != gamemanger.OwnerColor)
            endTEXT.text = "GameOver";
        else
            endTEXT.text = "GameWin";
    }
    public void BackMenu()
    {
        Loadingmanager.LoadScene("MenuSene");
    }
    public void ReGame()
    {
        Loadingmanager.LoadScene("GameScene");
    }
    public void LoadGame()
    {

    }
}
