using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameMenu : UI
{
     public void OpenSaveUI()
    {
        UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
        uimanager.OpenUI(UItype.INGAMESAVE);
    }
    public void GoToTitle()
    {
        Loadingmanager.LoadScene("MenuSene");
    }
}
