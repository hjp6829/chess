using Photon.Pun;
using Photon.Pun.Demo.PunBasics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnPromotionUI : UI
{
    private Piece pawn;
    private PhotonView photonView;
    public override void Open()
    {
        base.Open();
        photonView = GetComponent<PhotonView>();
        if (Gamemanager.IsNetworkConnect)
        {
            Networkmanager netmanager=ServiceLocatorManager.GetService<Networkmanager>();
            netmanager.GamePause(true);
            return;
        }
        GamePause(true);
    }
    private void GamePause(bool pause)
    {
        Debug.Log("����");
        Gamemanager.IsPause = pause;
    }

    public void SetPromotionPawn(Piece piece)
    {
        pawn = piece;
    }
    public void PromotionQueen()
    {
        ChangePawnToOther((int)ChessPiece.QUEEN);
    }
    public void PromotionKnight()
    {
        ChangePawnToOther((int)ChessPiece.KNIGHTS);
    }
    public void PromotionRook()
    {
        ChangePawnToOther((int)ChessPiece.ROOKS);
    }
    public void PromotionBishop()
    {
        ChangePawnToOther((int)ChessPiece.BISHOPS);
    }
    private void ChangePawnToOther(int type)
    {
        ChessDatamanager datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        datamanager.ChangeData(pawn.TilePos, (ChessPiece)type, pawn.PieceColor);
        Close();
    }
    public override void Close()
    {
        base.Close();
        if (Gamemanager.IsNetworkConnect)
        {
            Networkmanager netmanager = ServiceLocatorManager.GetService<Networkmanager>();
            netmanager.GamePause(false);
            return;
        }
        GamePause(false);
    }
}
