using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InGameMenuButton : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI buttonText;
    private bool isClick;
    private void Start()
    {
        UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
        uimanager.GetUI<UI>(UItype.INGAMEMENU).OnCloseAction += OnCloseMenu;
    }
    public void ToggleMenu()
    {
        UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
        if (!isClick)
        {
            uimanager.OpenUI(UItype.INGAMEMENU);
            isClick = true;
            buttonText.text = "MenuClose";
            return;
        }
        uimanager.CloseUI(UItype.INGAMEMENU);
        isClick = false;
        buttonText.text = "MenuOpen";
    }
    public void OnCloseMenu()
    {
        isClick = false;
        buttonText.text = "MenuOpen";
    }
}
