using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class DiedPieceUI : UI
{
    [SerializeField] private TextMeshProUGUI whiteDiedTEXT;
    [SerializeField] private TextMeshProUGUI blackDiedTEXT;

    private void Start()
    {
        ChessDatamanager datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        datamanager.OnUpdateDiedPiece += (Dictionary<ChessColor, Dictionary<ChessPiece, int>>data) =>
        {
            foreach(var Colordata in data)
            {
                TextMeshProUGUI tempTEXT = GetColorTEXT(Colordata.Key);
                Colordata.Value.OrderBy(x => x.Key);
                SetDataToString(Colordata.Value, tempTEXT);
            }
        };
    }
    private TextMeshProUGUI GetColorTEXT(ChessColor color)
    {
        switch(color)
        {
            case ChessColor.BLACK: 
                return blackDiedTEXT;
            case ChessColor.WHITE:
                return whiteDiedTEXT;
        }
        return null;
    }
    private void SetDataToString(Dictionary<ChessPiece, int>data, TextMeshProUGUI testmesh)
    {
        testmesh.text = "";
        foreach (var colordata in data)
        {
            testmesh.text= testmesh.text+ colordata.Key+" : "+ colordata.Value.ToString()+"\n";
        }
    }
}
