using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameLoadButton : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI buttonName;
    private SaveData saveData;
    public void SetGameData(SaveData data)
    {
        saveData = data;
        buttonName.text = saveData.saveName;
    }
}
