using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameLoadUI : UI
{
    [SerializeField] private GameObject viewContent;
    [SerializeField] private GameLoadButton loadbutton;
    [SerializeField] private TextMeshProUGUI loadGameName;
    [SerializeField] private Image previewImage;
    private SaveData selectData;
    private List<GameObject> buttonList = new List<GameObject>();
    public override void Open()
    {
        base.Open();
        previewImage.sprite = null;
        for (int i=0;i<buttonList.Count;i++)
        {
            Destroy(buttonList[i]);
        }
        buttonList.Clear();
        Tilemanager tilemanager = ServiceLocatorManager.GetService<Tilemanager>();
        ChessDatamanager datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        List<SaveData> list = datamanager.SaveList.list;
        if (list == null)
            return;
        foreach(SaveData data in list)
        {
            GameLoadButton temp = Instantiate(loadbutton);
            temp.SetGameData(data);
            temp.GetComponent<Button>().onClick.AddListener(() =>
            {
                selectData = data;
                loadGameName.text = data.saveName;
                previewImage.sprite=datamanager.LoadSaveImage(data.saveName);
            });
            temp.transform.SetParent(viewContent.transform);
            buttonList.Add(temp.gameObject);
        }
    }
    public void LoadGame()
    {
        Loadingmanager.LoadScene("GameScene", () =>
        {
            ServiceLocatorManager.GetService<Gamemanager>().OnGameLoad(selectData);
        });
        ServiceLocatorManager.GetService<ChessDatamanager>().LoadGameData(selectData.saveName);
    }
    public void DeleteSaveData()
    {
        ServiceLocatorManager.GetService<ChessDatamanager>().DeleteJsonData(selectData.saveName);
        Open();
    }
}
