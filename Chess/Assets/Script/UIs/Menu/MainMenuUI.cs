using Photon.Pun.Demo.PunBasics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuUI : UI
{
   public void GameStart()
    {
        Gamemanager gamemanager = ServiceLocatorManager.GetService<Gamemanager>();
        Loadingmanager.LoadScene("GameScene", () =>
        {
            gamemanager.OnGameStart?.Invoke();
        });
    }
    public void GameLoad()
    {
        UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
        uimanager.OpenUI(UItype.GAMELOAD);
    }
    public void MultiPlayStart()
    {
        Loadingmanager.LoadScene("MulitScene");
    }
}
