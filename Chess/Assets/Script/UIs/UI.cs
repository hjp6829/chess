using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class UI : MonoBehaviour
{
    [HideInInspector] public Action OnCloseAction;
    [SerializeField] private UItype uitype;
    [SerializeField] private bool isActiveESC=true;

    public bool IsActiveESC { get => isActiveESC; }
    public UItype UIType { get => uitype; }
    public virtual void Open()
    {
        gameObject.SetActive(true);
    }
    public virtual void Close()
    {
        gameObject.SetActive(false);
        OnCloseAction?.Invoke();
    }
    public virtual void initialze()
    {

    }
}
