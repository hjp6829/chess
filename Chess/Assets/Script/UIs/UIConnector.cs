using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
public class UIConnector : SerializedMonoBehaviour
{
    [SerializeField] private List<UI> uis=new List<UI>();
    private void Start()
    {
        UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
        uimanager.UIConnector = this;
        uimanager.ResetUIStack();
        foreach (var ui in uis)
        {
            ui.initialze();
        }
    }
    public UI GetUI(UItype uitype)
    {
        return uis.Find(x => x.UIType == uitype);
    }
}
