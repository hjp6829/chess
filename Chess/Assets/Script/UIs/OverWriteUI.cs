using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverWriteUI : UI
{
    [HideInInspector] public Action OnAccept;

    public void Accept()
    {
        OnAccept?.Invoke();
        Close();
    }
}
