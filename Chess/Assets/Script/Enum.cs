public enum ChessColor
{
    NONE,
    BLACK,
    WHITE
} 
public enum ChessPiece
{
    NONE,
    KING,
    QUEEN,
    ROOKS,
    KNIGHTS,
    BISHOPS,
    PAWNS
}
public enum UItype
{
    GAMELOAD,
    GAMEEND,
    MAINMENU,
    CONNECTING,
    MULTIROOMLIST,
    ROOM,
    INGAMEMENU,
    INGAMESAVE,
    PAWNPROMOTION,

}
public enum CameraType
{
    MAIN,
    PREVIEW
}
