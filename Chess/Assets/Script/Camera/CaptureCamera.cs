using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class CaptureCamera : MonoBehaviour
{
    private Camera captureCamera;
    private RenderTexture captureTexture;
    private Action<string> captureAction;
    private ChessDatamanager datamanager;
    private void Start()
    {
        datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        captureAction += (string name) =>
        {
            Capture(name);
        };
        datamanager.OnJsonSave += captureAction;
        captureCamera = GetComponent<Camera>();
        captureTexture = captureCamera.targetTexture;
    }
    public void Capture(string imagename)
    {
        Texture2D texture = new Texture2D(captureTexture.width, captureTexture.height, TextureFormat.RGB24, false);
        RenderTexture.active = captureTexture;
        texture.ReadPixels(new Rect(0, 0, captureTexture.width, captureTexture.height), 0, 0);
        texture.Apply();
        datamanager.SaveCapture(texture, imagename);
        RenderTexture.active = null;
    }
    private void OnDestroy()
    {
        datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        datamanager.OnJsonSave -= captureAction;
    }
}
