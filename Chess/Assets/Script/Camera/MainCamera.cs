using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    private void Start()
    {
        Cameramanager cameramanager = ServiceLocatorManager.GetService<Cameramanager>();
        cameramanager.GameCamera = GetComponent<Camera>();
    }
}
