using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Burst.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

public class CameraRay : MonoBehaviour
{
    private Camera gameCamera;
    [SerializeField] private GameObject test;
    private Piece selecPiece;
    private List<GameObject> canMovePosObject= new List<GameObject>();
    private List<Vector2Int> canMovePos= new List<Vector2Int>();
    // Start is called before the first frame update
    void Start()
    {
        gameCamera=GetComponent<Camera>();
    }
    private void ClickTargetPiece(RaycastHit2D hit)
    {
        ChessDatamanager datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        if (ServiceLocatorManager.GetService<Gamemanager>().ChessTurnColor != hit.transform.GetComponent<Piece>().PieceColor)
        {
            if (selecPiece == null)
                return;
            Piece targetPiece = hit.transform.GetComponent<Piece>();
            if (datamanager.GetChessTileData(targetPiece.TilePos).pieceObject == null)
                return;
            if (!canMovePos.Contains(targetPiece.TilePos))
                return;
            selecPiece.IsActive = true;
            datamanager.DetroyPiece(new Vector2((int)targetPiece.TilePos.x, (int)targetPiece.TilePos.y));
            datamanager.UpdatePos(selecPiece.TilePos, targetPiece.TilePos);
            selecPiece = null;
            if (ServiceLocatorManager.GetService<Gamemanager>().ChessTurnColor == ChessColor.WHITE)
                ServiceLocatorManager.GetService<Gamemanager>().ChessTurnColor = ChessColor.BLACK;
            else
                ServiceLocatorManager.GetService<Gamemanager>().ChessTurnColor = ChessColor.WHITE;
            for (int i = 0; i < canMovePosObject.Count; i++)
            {
                Destroy(canMovePosObject[i]);
            }
            canMovePosObject.Clear();
            return;
        }
        ///캐슬링 때문에 추가
        if (selecPiece != null && selecPiece is King && (selecPiece as King).CanCastling)
        {
            Piece targetPiece = hit.transform.GetComponent<Piece>();
            King king = selecPiece as King;
            if (targetPiece is Rook)
            {
                king.MakeCastlingPos(selecPiece.TilePos, targetPiece.TilePos);
                datamanager.UpdatePos(selecPiece.TilePos, king.CastlingPos);
                datamanager.UpdatePos(targetPiece.TilePos, king.CastlingRookPos);
                selecPiece.IsActive = true;
                selecPiece = null;
                if (ServiceLocatorManager.GetService<Gamemanager>().ChessTurnColor == ChessColor.WHITE)
                    ServiceLocatorManager.GetService<Gamemanager>().ChessTurnColor = ChessColor.BLACK;
                else
                    ServiceLocatorManager.GetService<Gamemanager>().ChessTurnColor = ChessColor.WHITE;
                for (int i = 0; i < canMovePosObject.Count; i++)
                {
                    Destroy(canMovePosObject[i]);
                }
                canMovePosObject.Clear();
                return;
            }
        }
        for (int i = 0; i < canMovePosObject.Count; i++)
        {
            Destroy(canMovePosObject[i]);
        }
        canMovePosObject.Clear();
        canMovePos.Clear();
        selecPiece = hit.transform.GetComponent<Piece>();
        canMovePos = selecPiece.MakeMovePos();
        foreach (Vector2Int pos in canMovePos)
        {
            GameObject temp = Instantiate(test, datamanager.GetChessTileData(pos).tilePos, test.transform.rotation);
            temp.GetComponent<SpriteRenderer>().sortingOrder = 1;
            canMovePosObject.Add(temp);
        }
    }
    private void SelectTargetTile(RaycastHit2D hit)
    {
        ChessDatamanager datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        if (selecPiece != null)
        {
            Vector2Int tilePos = hit.transform.GetComponent<TileData>().TilePos;
            if (!canMovePos.Contains(tilePos))
                return;
            selecPiece.IsActive = true;
            datamanager.UpdatePos(selecPiece.TilePos, tilePos);
            selecPiece = null;
            if (ServiceLocatorManager.GetService<Gamemanager>().ChessTurnColor == ChessColor.WHITE)
                ServiceLocatorManager.GetService<Gamemanager>().ChessTurnColor = ChessColor.BLACK;
            else
                ServiceLocatorManager.GetService<Gamemanager>().ChessTurnColor = ChessColor.WHITE;
            for (int i = 0; i < canMovePosObject.Count; i++)
            {
                Destroy(canMovePosObject[i]);
            }
            canMovePosObject.Clear();
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Gamemanager.IsPause)
                return;
            Vector3 mousePos = gameCamera.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(mousePos, transform.forward, 100);
           
            if (hit)
            {
                switch (hit.transform.gameObject.layer)
                {
                    case 6:
                        ClickTargetPiece(hit);
                        break;
                    case 7:
                        SelectTargetTile(hit);
                        break;
                }

            }
        }

    }
}
