using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.Linq;

public class testMultiRay : MonoBehaviour
{
    private Camera gameCamera;
    [SerializeField] private GameObject test;
    private Piece selecPiece;
    private List<GameObject> canMovePosObject = new List<GameObject>();
    private List<Vector2Int> canMovePos = new List<Vector2Int>();
    private PhotonView photonView;
    private ChessDatamanager datamanager;
    // Start is called before the first frame update
    void Start()
    {
        gameCamera = GetComponent<Camera>();
        photonView = GetComponent<PhotonView>();
        datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
    }
    [PunRPC]
    private void UpdatePiece(Vector2 targetPos)
    {
        datamanager.UpdatePos(selecPiece.TilePos, new Vector2Int((int)targetPos.x, (int)targetPos.y));
        selecPiece.IsActive = true;
        selecPiece = null;
        Gamemanager gamemanager = ServiceLocatorManager.GetService<Gamemanager>();
        if (gamemanager.ChessTurnColor == ChessColor.WHITE)
            gamemanager.ChessTurnColor = ChessColor.BLACK;
        else
            gamemanager.ChessTurnColor = ChessColor.WHITE;
    }
    [PunRPC]
    private void UpdatePiece(Vector2 selecpiece, Vector2 targetPos)
    {
        datamanager.UpdatePos(new Vector2Int((int)selecpiece.x, (int)selecpiece.y), new Vector2Int((int)targetPos.x, (int)targetPos.y));
    }
    [PunRPC]
    private void DestroyPiece(Vector2 targetPos)
    {
        datamanager.DetroyPiece(targetPos);
    }
    [PunRPC]
    private void SelectPiece(Vector2 selectPos)
    {
        Vector2Int targetPosVector2Int = new Vector2Int((int)selectPos.x, (int)selectPos.y);
        selecPiece = datamanager.GetChessTileData(targetPosVector2Int).pieceObject;
    }
    private void ClickTargetPiece(Piece targetpiece)
    {
        ChessDatamanager datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        if (ServiceLocatorManager.GetService<Gamemanager>().ChessTurnColor != targetpiece.PieceColor)
        {
            if (selecPiece == null)
                return;
            if (datamanager.GetChessTileData(targetpiece.TilePos).pieceObject == null)
                return;
            if (!canMovePos.Contains(targetpiece.TilePos))
                return;
            
            photonView.RPC("DestroyPiece", RpcTarget.All, new Vector2(targetpiece.TilePos.x, targetpiece.TilePos.y) );
            photonView.RPC("UpdatePiece", RpcTarget.All, new Vector2(targetpiece.TilePos.x, targetpiece.TilePos.y));
            for (int i = 0; i < canMovePosObject.Count; i++)
            {
                Destroy(canMovePosObject[i]);
            }
            canMovePosObject.Clear();
            return;
        }
        if (selecPiece != null && selecPiece is King && (selecPiece as King).CanCastling)
        {
            King king = selecPiece as King;
            if (targetpiece is Rook)
            {
                king.MakeCastlingPos(selecPiece.TilePos, targetpiece.TilePos);
                photonView.RPC("UpdatePiece", RpcTarget.All, new Vector2(king.CastlingPos.x, king.CastlingPos.y));
                photonView.RPC("UpdatePiece", RpcTarget.All, new Vector2(targetpiece.TilePos.x, targetpiece.TilePos.y), new Vector2(king.CastlingRookPos.x, king.CastlingRookPos.y));
                for (int i = 0; i < canMovePosObject.Count; i++)
                {
                    Destroy(canMovePosObject[i]);
                }
                canMovePosObject.Clear();
                return;
            }
        }
        for (int i = 0; i < canMovePosObject.Count; i++)
        {
            Destroy(canMovePosObject[i]);
        }
        canMovePosObject.Clear();
        canMovePos.Clear();
        photonView.RPC("SelectPiece", RpcTarget.All, new Vector2(targetpiece.TilePos.x, targetpiece.TilePos.y));
        canMovePos = selecPiece.MakeMovePos();
        foreach (Vector2Int pos in canMovePos)
        {
            GameObject temp = Instantiate(test, datamanager.GetChessTileData(pos).tilePos, test.transform.rotation);
            temp.GetComponent<SpriteRenderer>().sortingOrder = 1;
            canMovePosObject.Add(temp);
        }
    }
    private void SelectTargetTile(TileData tileTemp)
    {
        if (selecPiece != null)
        {
            if (!canMovePos.Contains(tileTemp.TilePos))
                return;
            photonView.RPC("UpdatePiece", RpcTarget.All, new Vector2(tileTemp.TilePos.x, tileTemp.TilePos.y));

            for (int i = 0; i < canMovePosObject.Count; i++)
            {
                Destroy(canMovePosObject[i]);
            }
            canMovePosObject.Clear();
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Gamemanager.IsPause)
                return;
            Vector3 mousePos = gameCamera.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(mousePos, transform.forward, 100);
            Gamemanager gamemanager = ServiceLocatorManager.GetService<Gamemanager>();
            if (gamemanager.OwnerColor != gamemanager.ChessTurnColor)
                return;
            if (hit)
            {
                switch (hit.transform.gameObject.layer)
                {
                    case 6:
                        Piece pieceTemp = hit.transform.GetComponent<Piece>();
                        ClickTargetPiece(pieceTemp);
                        break;
                    case 7:
                        TileData tileTemp = hit.transform.GetComponent<TileData>();
                        SelectTargetTile(tileTemp);
                        break;
                }

            }
        }

    }
}
