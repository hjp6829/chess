using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class Tilemanager : SerializedMonoBehaviour,IService
{
    [SerializeField] private int widthumber;
    [SerializeField] private int lengthnumber;
    [SerializeField] private GameObject tile;
    Dictionary<int, List<TileData>> mapdata = new Dictionary<int, List<TileData>>();
    public void Register()
    {
       ServiceLocatorManager.Register(this);
    }

    private void Awake()
    {
        Register();
    }
    // Start is called before the first frame update
    void Start()
    {
        Gamemanager gamemanager = ServiceLocatorManager.GetService<Gamemanager>();
        gamemanager.OnGameStart += () =>
        {
            mapdata.Clear();
            MakeChessBoard();
        };
        gamemanager.OnGameLoad += (SaveData data) =>
        {
            MakeLoadTile(data.LoadArry());
        };
        gamemanager.OnMultiGameStart += () =>
        {
            MakeChessBoard();
        };
        gamemanager.OnMultiGameReset += () =>
        {
            foreach(var data in mapdata.Values)
            {
                for(int i=0;i<data.Count; i++)
                {
                    Destroy(data[i].gameObject);
                }
                data.Clear();
            }
            mapdata.Clear();
        };
    }
    public Dictionary<int, List<TileData>> MakeTile(Vector2 cneterPos)
    {
        int boxWidthSize = (int)tile.transform.localScale.x;
        int boxLengthSize = (int)tile.transform.localScale.y;
        Vector2 installBoxCenter = cneterPos;
        for(int i=0;i<8;i++)
        {
            List<TileData> temp = new List<TileData>();
            for (int j=0;j<8;j++)
            {
                TileData tiledata = Instantiate(tile, installBoxCenter, tile.transform.rotation).GetComponent<TileData>();
                installBoxCenter.x += boxWidthSize;
                tiledata.Initialize(i, j);
                if ((j + i) % 2 == 0)
                    tiledata.SetTileColor(ChessColor.BLACK);
                else
                    tiledata.SetTileColor(ChessColor.WHITE);
                temp.Add(tiledata);
            }
            installBoxCenter.y += boxLengthSize;
            installBoxCenter.x = cneterPos.x;
            mapdata.Add(i, temp);
        }
        return mapdata;
    }
    private void MakeLoadTile(ChessTileData[,] tileArry)
    {
        Dictionary<int, List<TileData>> mapdata = MakeTile(transform.position);
        ChessDatamanager datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                ChessPiece piecetype = (ChessPiece)tileArry[i,j].pieceType;
                if (piecetype == ChessPiece.NONE)
                    continue;
                ChessColor pieceColor= (ChessColor)tileArry[i, j].pieceColor;
                Piece pieceTemp = datamanager.ChesspieceDic[pieceColor][piecetype];
                Vector3 Temp = new Vector3(mapdata[i][j].transform.position.x, mapdata[i][j].transform.position.y, -1);
                Piece piece = Instantiate(pieceTemp, Temp, mapdata[i][j].transform.rotation);
                piece.TilePos = new Vector2Int(i, j);
                piece.PieceColor = tileArry[i, j].pieceColor;
                piece.IsActive = tileArry[i, j].isActive;
                datamanager.SetChessTileData(piece.TilePos,piece);
            }
        }
    }
    private void MakeChessBoard()
    {
        Dictionary<int, List<TileData>> mapdata = MakeTile(transform.position);
        ChessDatamanager datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                ChessPiece piecetype = datamanager.TilePieceData[i][j];
                if (piecetype == ChessPiece.NONE)
                {
                    datamanager.SetChessTileData(new Vector2Int(i, j), ChessPiece.NONE, ChessColor.NONE, mapdata[i][j].transform.position, null);
                    continue;
                }
                ChessColor chessColor = (i < 2) ? ChessColor.WHITE : ChessColor.BLACK;
                Piece pieceTemp = datamanager.ChesspieceDic[chessColor][piecetype];
                Vector3 Temp = new Vector3(mapdata[i][j].transform.position.x, mapdata[i][j].transform.position.y, -1);
                Piece piece = Instantiate(pieceTemp, Temp, mapdata[i][j].transform.rotation);
                piece.TilePos = new Vector2Int(i, j);
                piece.PieceColor = chessColor;
                datamanager.SetChessTileData(piece.TilePos, piecetype, chessColor, mapdata[i][j].transform.position, piece);
            }
        }
    }
}
