using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UImanager : SerializedMonoBehaviour,IService
{
    private UIConnector uIConnector;
    private Stack<UI> OpenedUIs=new Stack<UI>();

    public UIConnector UIConnector { get { return uIConnector; } set => uIConnector = value; }
    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }
    private void Awake()
    {
        Register();
    }
    public void OpenUI(UItype uItype)
    {
        UI temp = uIConnector.GetUI(uItype);
        if(temp.IsActiveESC)
            OpenedUIs.Push(temp);
        temp.Open();
    }
    public void CloseUI(UItype uItype)
    {
        UI temp = uIConnector.GetUI(uItype);
        temp.Close();
        if (temp.IsActiveESC)
            OpenedUIs.Pop();
    }
    public void ToggleUI(UItype uItype)
    {
        UI uiTemp = uIConnector.GetUI(uItype);
        if (uiTemp.gameObject.activeSelf)
        { 
            uiTemp.Close();
            return;
        }
        uiTemp.Open();
    }
    public void ResetUIStack()
    {
        OpenedUIs.Clear();
    }
    public void ESCUI()
    {
        if(OpenedUIs.Count>0)
            OpenedUIs.Pop().Close();
    }
    public T GetUI<T>(UItype uItype) where T :UI
    {
        return (T)uIConnector.GetUI(uItype);
    }
}

