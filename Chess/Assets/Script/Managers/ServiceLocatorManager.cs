﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class ServiceLocatorManager
{
    private static readonly Dictionary<Type, SerializedMonoBehaviour> _dicServices = new Dictionary<Type, SerializedMonoBehaviour>();

    public static bool Register<T>(T registerService) where T : SerializedMonoBehaviour, IService
    {
        var varServiceType = typeof(T);
        if (_dicServices.ContainsKey(varServiceType) == true)
        {
            // Debug ��� ������.
            return false;
        }

        else
        {
            _dicServices.Add(varServiceType, registerService);
            return true;
        }
    }
    public static T GetService<T>() where T : SerializedMonoBehaviour, IService
    {
        var varServiceType = typeof(T);
        if (_dicServices.ContainsKey(varServiceType) == true)
        {
            return (T)_dicServices[varServiceType];
        }

        else
        {
            throw new ServiceLocatorException($"{varServiceType}�� ��ϵǾ� ���� �ʽ��ϴ�");
        }
    }
    public static void ReplaceService<T>(T replaceService) where T : SerializedMonoBehaviour, IService
    {
        var varServiceType = typeof(T);
        if (_dicServices.ContainsKey(varServiceType) == true)
        {
            _dicServices[varServiceType] = replaceService;
        }

        else
        {
            Register(replaceService);
        }
    }
    public static void DeleteService<T>() where T : SerializedMonoBehaviour, IService
    {
        var varServiceType = typeof(T);
        if (_dicServices.ContainsKey(varServiceType) == true)
        {
            _dicServices.Remove(varServiceType);
        }

        else
        {
            throw new ServiceLocatorException($"{varServiceType}�� ��ϵǾ� ���� �ʽ��ϴ�");
        }
    }
    public static void AutoDeleteManager()
    {
        foreach (var Service in _dicServices.ToList())
        {
            if (Service.Value == null)
            {
                Debug.Log(Service.Key);
                _dicServices.Remove(Service.Key);
            }
        }
    }
}
public class ServiceLocatorException : Exception
{
    public ServiceLocatorException(string strMsg) : base(strMsg) { }
}
