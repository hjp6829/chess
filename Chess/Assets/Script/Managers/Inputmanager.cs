using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Inputmanager : SerializedMonoBehaviour, IService
{
    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }
    private void Awake()
    {
        Register();
    }
    public void OnESC(InputValue value)
    {
        UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
        uimanager.ESCUI();
    }
}
