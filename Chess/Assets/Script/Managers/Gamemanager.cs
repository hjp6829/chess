using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using Photon.Pun;

public class Gamemanager : SerializedMonoBehaviour,IService
{
    public static bool IsPause;
    public static bool IsNetworkConnect;
    [HideInInspector] public Action OnGameStart;
    [HideInInspector] public Action<SaveData> OnGameLoad;
    [HideInInspector] public Action OnMultiGameStart;
    [HideInInspector] public Action OnMultiGameReset;
    [HideInInspector] public Action OnMultiGameLeave;

    private ChessColor chessTurnColor;
    private ChessColor winColor;
    private ChessColor ownerColor;

    public ChessColor OwnerColor { get => ownerColor; set => ownerColor = value; }
    public ChessColor WinColor { get => winColor; set => winColor = value; }
    public ChessColor ChessTurnColor { get => chessTurnColor; set => chessTurnColor = value; }
    public void Register()
    {
        if(!ServiceLocatorManager.Register(this))
            Destroy(this.gameObject);
    }
    private void Awake()
    {
        Register();
        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        OnGameStart += () =>
        {
            this.chessTurnColor= ChessColor.WHITE;
            ownerColor = ChessColor.WHITE;
        };
        OnGameLoad += (SaveData data) =>
        {
            ownerColor = (ChessColor)data.ownerColor;
            chessTurnColor = (ChessColor)data.chessTurnColor;
        };
        OnMultiGameStart += () =>
        {
            chessTurnColor = ChessColor.WHITE;
        };
    }
}
