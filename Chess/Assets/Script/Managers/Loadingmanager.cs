using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;
using System;
using Unity.VisualScripting;

public class Loadingmanager : SerializedMonoBehaviour
{
    [HideInInspector] public static Action loadAction;
    //[SerializeField]
    //private Image progressBar;
    //[SerializeField]
    //private float DelayTimer;
    public static string NextSceneName;

    private void Start()
    {
        StartCoroutine(LoadScene(loadAction));
        ServiceLocatorManager.AutoDeleteManager();
    }
    public static void LoadScene(string name, Action action = null)
    {
        Time.timeScale = 1;
        NextSceneName = name;
        SceneManager.LoadScene("LoadingScene");
        loadAction = action;
    }
    private void activeAction(AsyncOperation asyncOperation)
    {
        loadAction?.Invoke();
    }
    IEnumerator LoadScene(Action action = null,float delay=0)
    {
        AsyncOperation op = SceneManager.LoadSceneAsync(NextSceneName);
        op.completed += activeAction;
        op.allowSceneActivation = false;
        float timer = 0.0f;
        while (!op.isDone)
        {
            yield return null;
            timer += 0.1f * Time.deltaTime;
            delay -= Time.deltaTime;
            if (delay <= 0)
            {
                op.allowSceneActivation = true;
                yield break;
            }
        }
    }

}