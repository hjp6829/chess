using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Data;
using Photon.Pun;
using Photon.Pun.Demo.PunBasics;

[Serializable]
public class ChessTileData
{
    public Vector2 tilePos;
    public ChessPiece pieceType;
    public ChessColor pieceColor;
    public Piece pieceObject;
    public bool isActive;
}
#region jsonData
[Serializable]
public class SaveListData
{
    public int NewSaveNum;
    public List<SaveData> list = new List<SaveData>();
    public void AddNewSvae(SaveData data)
    {
        if (list.Count != 0)
            NewSaveNum = list[list.Count - 1].saveIDX;
        else
            NewSaveNum = 0;
        data.saveIDX = ++NewSaveNum;
        list.Add(data);
    }
    public void Deletedata(string saveName)
    {
        list.Remove(GetSavedData(saveName));
    }
    public bool isContain(string saveName)
    {
        return list.Exists(x=>x.saveName == saveName);
    }
    public SaveData GetSavedData(string saveName)
    {
        return list.Find(x=>x.saveName == saveName);
    }
    public void UpdateSaveData(SaveData data)
    {
        SaveData temp = GetSavedData(data.saveName);
        temp.saveName=data.saveName;
        temp.saveTime=data.saveTime;
        temp.ownerColor=data.ownerColor;
        temp.saveIDX = data.saveIDX;
        temp.tiledata = data.tiledata;
    }
}
[Serializable]
public class SaveData
{
    public string saveName;
    public string saveTime;
    public int ownerColor;
    public int chessTurnColor;
    public int saveIDX;
    public List<saveArry> tiledata;
    public void SetArry(ChessTileData[,] data)
    {
        tiledata = new List<saveArry>();
        for (int i=0;i<8;i++)
        {
            saveArry saveArry = new saveArry();
            saveArry.HeightIDX = i;
            saveArry.data = new List<ChessTileData>();
            for (int j = 0; j < 8; j++)
            {
                if(data[i, j].pieceObject!=null)
                    data[i, j].isActive = data[i, j].pieceObject.IsActive;
                saveArry.data.Add(data[i, j]);
            }
            tiledata.Add(saveArry);
        }
    }
    public ChessTileData[,] LoadArry()
    {
        ChessTileData[,] chessTileDatas = new ChessTileData[8, 8];
        foreach(var data in tiledata)
        {
            for(int i=0;i<8;i++)
            {
                chessTileDatas[data.HeightIDX, i] = data.data[i];
            }
        }
        return chessTileDatas;
    }
}
[Serializable]
public class saveArry
{
    public int HeightIDX;
    public List<ChessTileData> data;
}
#endregion
public class ChessDatamanager : SerializedMonoBehaviour, IService
{
    [HideInInspector] public Action<string> OnJsonSave;
    private string JSONPATH = Path.Combine(Application.dataPath, "DB.json");
    SaveData saveData;
    #region inGame
    [HideInInspector] public Action<Dictionary<ChessColor, Dictionary<ChessPiece, int>>> OnUpdateDiedPiece;
    [SerializeField]
    private Dictionary<ChessColor, Dictionary<ChessPiece, Piece>> chesspieceDic = new Dictionary<ChessColor, Dictionary<ChessPiece, Piece>>();
    [SerializeField]
    private Dictionary<int, List<ChessPiece>> tilePieceData = new Dictionary<int, List<ChessPiece>>();
    private ChessTileData[,] chessTileDatas;
    private Dictionary<ChessColor, Dictionary<ChessPiece, int>> diedPiece = new Dictionary<ChessColor, Dictionary<ChessPiece, int>>();
    private SaveListData saveList=new SaveListData();
    private PhotonView photonView;

    public SaveListData SaveList { get => saveList; }
    public ChessTileData[,] ChessTileDatas { get => chessTileDatas; }
    public Dictionary<int, List<ChessPiece>> TilePieceData { get => tilePieceData; }
    public Dictionary<ChessColor, Dictionary<ChessPiece, Piece>> ChesspieceDic { get => chesspieceDic; }
    #endregion

    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }
    private void Awake()
    {
        Register();
        chessTileDatas = new ChessTileData[8, 8];
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                chessTileDatas[i, j] = new ChessTileData();
            }
        }
    }
    private void Start()
    {
        //JsonSave();
        
        JsonLoad();
        Gamemanager gamemanager = ServiceLocatorManager.GetService<Gamemanager>();
        gamemanager.OnMultiGameReset += () =>
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    DetroyPiece(new Vector2(i, j));
                }
            }
        };
        gamemanager.OnMultiGameStart += () =>
        {
            AddPhotonView();
        };
        gamemanager.OnMultiGameLeave += () =>
        {
            Destroy(photonView);
        };
    }
    void AddPhotonView()
    {
        this.gameObject.AddComponent<PhotonView>();
        photonView = GetComponent<PhotonView>();
    }
    #region inGame
    public void SetChessTileData(Vector2Int tileIDX, ChessPiece pieceType, ChessColor pieceColor, Vector2 tilePos,Piece piece)
    {
        chessTileDatas[tileIDX.x, tileIDX.y].tilePos = tilePos;
        chessTileDatas[tileIDX.x, tileIDX.y].pieceType = pieceType;
        chessTileDatas[tileIDX.x, tileIDX.y].pieceColor = pieceColor;
        chessTileDatas[tileIDX.x, tileIDX.y].pieceObject = piece;
    }
    public void SetChessTileData(Vector2Int tileIDX,Piece piece)
    {
        chessTileDatas[tileIDX.x, tileIDX.y].pieceObject = piece;
    }
    public void SetChessTileData(Vector2Int tileIDX, Piece piece, ChessPiece pieceType, ChessColor pieceColor)
    {
        chessTileDatas[tileIDX.x, tileIDX.y].pieceObject = piece;
        chessTileDatas[tileIDX.x, tileIDX.y].pieceType = pieceType;
        chessTileDatas[tileIDX.x, tileIDX.y].pieceColor = pieceColor;
    }
    public void DetroyPiece(Vector2 tileIDX)
    {
        ChessTileData tempData = chessTileDatas[(int)tileIDX.x, (int)tileIDX.y];
        AddDiedPiece(tempData.pieceColor, tempData.pieceType);
        OnUpdateDiedPiece?.Invoke(diedPiece);
        if(chessTileDatas[(int)tileIDX.x, (int)tileIDX.y].pieceObject!=null)
         Destroy(chessTileDatas[(int)tileIDX.x, (int)tileIDX.y].pieceObject.gameObject);
        if (tempData.pieceType == ChessPiece.KING)
        {
            Gamemanager gamemanaget = ServiceLocatorManager.GetService<Gamemanager>();
            switch(tempData.pieceColor)
            {
                case ChessColor.BLACK:
                    gamemanaget.WinColor = ChessColor.WHITE;
                    break;
                case ChessColor.WHITE:
                    gamemanaget.WinColor = ChessColor.BLACK;
                    break;
            }
           
            UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
            //uimanager.GetUI(UItype.GAMEEND).Open();
        }
        chessTileDatas[(int)tileIDX.x, (int)tileIDX.y].pieceType = ChessPiece.NONE;
        chessTileDatas[(int)tileIDX.x, (int)tileIDX.y].pieceColor = ChessColor.NONE;
        chessTileDatas[(int)tileIDX.x, (int)tileIDX.y].pieceObject = null;
    }
    public void UpdatePos(Vector2Int ExistingLocation, Vector2Int NewPos)
    {
        ChessTileData tempData = chessTileDatas[ExistingLocation.x, ExistingLocation.y];
        Vector2 ExistingPos = chessTileDatas[ExistingLocation.x, ExistingLocation.y].tilePos;
        Vector2 newPos = chessTileDatas[NewPos.x, NewPos.y].tilePos;
        tempData.pieceObject.transform.position = newPos;
        chessTileDatas[ExistingLocation.x, ExistingLocation.y] = chessTileDatas[NewPos.x, NewPos.y];
        chessTileDatas[NewPos.x, NewPos.y] = tempData;
        chessTileDatas[NewPos.x, NewPos.y].pieceObject.TilePos = NewPos;
        chessTileDatas[NewPos.x, NewPos.y].tilePos= newPos;
        chessTileDatas[ExistingLocation.x, ExistingLocation.y].tilePos = ExistingPos;
        chessTileDatas[NewPos.x, NewPos.y].pieceObject.SetNewPos();
    }
    public void ChangeData(Vector2Int Pos,ChessPiece type,ChessColor chessColor)
    {
        if(Gamemanager.IsNetworkConnect)
        {
            Vector2 vector2Temp = new Vector2(Pos.x, Pos.y);
            photonView.RPC("ChangeDataPhoton", RpcTarget.All, vector2Temp, (int)type, (int)chessColor);
            return;
        }
        Debug.Log("pawn�ٸ��ɷ� �ٲ�");
        Destroy(chessTileDatas[Pos.x, Pos.y].pieceObject.gameObject);
        Piece pieceTemp = ChesspieceDic[chessColor][type];
        Piece piece = Instantiate(pieceTemp);
        piece.TilePos = Pos;
        piece.PieceColor = chessColor;
        piece.IsActive = true;
        piece.transform.position = chessTileDatas[Pos.x, Pos.y].tilePos;
        SetChessTileData(Pos, piece, type, chessColor);
    }
    [PunRPC]
    public void ChangeDataPhoton(Vector2 PosVector2, int type, int chessColor)
    {
        Vector2Int Pos = new Vector2Int((int)PosVector2.x, (int)PosVector2.y);
        Destroy(chessTileDatas[Pos.x, Pos.y].pieceObject.gameObject);
        Piece pieceTemp = ChesspieceDic[(ChessColor)chessColor][(ChessPiece)type];
        Piece piece = Instantiate(pieceTemp);
        piece.TilePos = Pos;
        piece.PieceColor = (ChessColor)chessColor;
        piece.IsActive = true;
        piece.transform.position = chessTileDatas[Pos.x, Pos.y].tilePos;
        SetChessTileData(Pos, piece, (ChessPiece)type, (ChessColor)chessColor);
        Debug.Log("pawn�ٸ��ɷ� �ٲ�");
    }
    public ChessTileData GetChessTileData(Vector2Int vector)
    {
        return chessTileDatas[vector.x, vector.y];
    }
    public void AddDiedPiece(ChessColor colortype,ChessPiece piecetype)
    {
        if (!diedPiece.ContainsKey(colortype))
        {
            Dictionary<ChessPiece, int> TempDic = new Dictionary<ChessPiece, int>();
            diedPiece.Add(colortype, TempDic);
        }
        if (!diedPiece[colortype].ContainsKey(piecetype))
        {
            diedPiece[colortype].Add(piecetype,1);
            return;
        }
        diedPiece[colortype][piecetype] += 1;
    }
    #endregion

    public bool CheckOverWrite(string name)
    {
        return saveList.isContain(name);
    }
    public void AddNewSave(string name)
    {
        saveData = new SaveData();
        saveData.saveTime = DateTime.Now.ToString("yyyy.MM.dd HH:mm");
        Gamemanager gamemanager = ServiceLocatorManager.GetService<Gamemanager>();
        saveData.ownerColor = (int)gamemanager.OwnerColor;
        saveData.chessTurnColor = (int)gamemanager.ChessTurnColor;
        saveData.SetArry(chessTileDatas);
        saveData.saveName = name;
        OnJsonSave?.Invoke(name);
        if (saveList.isContain(name))
            saveList.UpdateSaveData(saveData);
        else
            saveList.AddNewSvae(saveData);
        JsonSave();
    }
    public void JsonSave()
    {
        string file=JsonUtility.ToJson(saveList, true);
        File.WriteAllText(JSONPATH, file);
    }
    public void DeleteJsonData(string name)
    {
        DeleteCaptureImage(name);
        saveList.Deletedata(name);
        JsonSave();
    }
    public void JsonLoad()
    {
        if(File.Exists(JSONPATH))
        {
            string loadJson=File.ReadAllText(JSONPATH);
            saveList = JsonUtility.FromJson<SaveListData>(loadJson);
        }
    }
    public void LoadGameData(string name)
    {
        chessTileDatas = saveList.GetSavedData(name).LoadArry();
    }
    public HashSet<Vector2Int> CheckAttackTile(ChessColor chessColor)
    {
        HashSet<Vector2Int> Temp= new HashSet<Vector2Int>();
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (chessTileDatas[i, j].pieceColor != chessColor&& chessTileDatas[i, j].pieceColor!= ChessColor.NONE)
                {
                    Temp.UnionWith(chessTileDatas[i, j].pieceObject.ExceptionPos());
                }
            }
        }
        return Temp;
    }
    public void SaveCapture(Texture2D image,string name)
    {
        byte[] bytes = image.EncodeToPNG();
        File.WriteAllBytes("Assets/Image/SaveImages/" + name+".png", bytes);
    }
    public void DeleteCaptureImage(string name)
    {
        File.Delete("Assets/Image/SaveImages/" + name + ".png");
    }
    public Sprite LoadSaveImage(string name)
    {
        byte[] bytes = File.ReadAllBytes("Assets/Image/SaveImages/" + name + ".png");
        Texture2D texture = new Texture2D(0,0);
        texture.LoadImage(bytes);
        Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        return sprite;
    }
    public List<Piece> GetCastlingRooks(ChessColor chessColor)
    {
        List<Piece> tempList = new List<Piece>();
        for(int i=0;i< 8;i++)
        {
            for(int j=0;j<8;j++)
            {
                ChessTileData temp = chessTileDatas[i, j];
                if (temp.pieceColor == chessColor && temp.pieceType == ChessPiece.ROOKS)
                {
                    if(!temp.isActive)
                        tempList.Add(chessTileDatas[i, j].pieceObject); 
                } 
            }
        }
        return tempList;
    }
}
