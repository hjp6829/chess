using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using Sirenix.OdinInspector;
using System;

public class Networkmanager : MonoBehaviourPunCallbacks,IService
{
    [HideInInspector] public Action OnJoinRoom;
    [HideInInspector] public Action OnLeftPlayer;
    [HideInInspector] public Action<Player> OnJoinNewPlayer;
    [HideInInspector] public Action<Player> OnLeftNewPlayer;

    [SerializeField] private MultiRoomList testRoomList;

    private PhotonView photonview;
    private string userName;
    public string UserName { get => userName; }
    private void Awake()
    {
        Register();
        userName = "Player" + UnityEngine.Random.Range(0, 100);
    }
    private void Start()
    {
        photonview=GetComponent<PhotonView>();
        testRoomList.Open();
        Gamemanager.IsNetworkConnect = true;
        UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
        uimanager.OpenUI(UItype.CONNECTING);
        PhotonNetwork.LocalPlayer.NickName = userName;
        Connect();
    }
    public void SetNewUserName(string name)
    {
        userName = name;
        PhotonNetwork.LocalPlayer.NickName = userName;
    }
    public void Connect()
    {
        PhotonNetwork.Disconnect();
        PhotonNetwork.ConnectUsingSettings();
    }
    public override void OnJoinedLobby()
    {
        Debug.Log("로비에 들어감");
        UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
        uimanager.CloseUI(UItype.CONNECTING);
        uimanager.OpenUI(UItype.MULTIROOMLIST);

    }
    public override void OnConnectedToMaster()
    {
        Debug.Log("로비 접속중");
        PhotonNetwork.JoinLobby();
    }
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("룸 찾기를 실패했습니다");
        PhotonNetwork.CreateRoom(null);
    }
    public override void OnJoinedRoom()
    {
        OnJoinRoom?.Invoke();
        UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
        uimanager.CloseUI(UItype.CONNECTING);
        uimanager.OpenUI(UItype.ROOM);
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        OnJoinNewPlayer?.Invoke(newPlayer);
        if (PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers)
            PhotonNetwork.CurrentRoom.IsVisible = false;
        Debug.Log("새로운 플레이어가 입장했습니다 : " + newPlayer.NickName);
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        OnLeftNewPlayer?.Invoke(otherPlayer);
        if (PhotonNetwork.CurrentRoom.PlayerCount != PhotonNetwork.CurrentRoom.MaxPlayers)
            PhotonNetwork.CurrentRoom.IsVisible = true;
        Debug.Log("플레이어가 나갔습니다 : " + otherPlayer.NickName);
    }
    public void LeftRoom()
    {
        PhotonNetwork.LeaveRoom();
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);

        foreach (RoomInfo roomInfo in roomList)
        {
            if (roomInfo.RemovedFromList)
                testRoomList.DeleteRoomList(roomInfo);
            else
                testRoomList.CreateRoomList(roomInfo);
        }
    }
    public void CreateNewRoom(string roomname)
    {
        RoomOptions options = new RoomOptions();
        options.IsVisible = true;
        options.IsOpen = true;
        options.EmptyRoomTtl = 0;
        options.MaxPlayers = 2;
        PhotonNetwork.CreateRoom(roomname, options);
    }
    public bool PlayerCountChecker()
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount < 2)
            return false;
        return true;
    }
    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        Debug.Log("방생성 성공");
    }
    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }
    public void GamePause(bool pause)
    {
        photonview.RPC("GamePausePhoton", RpcTarget.All, pause);
    }
    [PunRPC]
    private void GamePausePhoton(bool pause)
    {
        Gamemanager.IsPause = pause;
    }
}
