using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cameramanager : SerializedMonoBehaviour, IService
{
    private Camera gameCamera;

    public Camera GameCamera { get { return gameCamera; } set => gameCamera = value; }
    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }
    private void Awake()
    {
        Register();
    }
}
