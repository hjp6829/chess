using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;

public class TileData : MonoBehaviour
{
    private SpriteRenderer render;
    private Vector2Int tilePos;

    public Vector2Int TilePos { get => tilePos; }
    public void Initialize(int H, int W)
    {
        render = GetComponent<SpriteRenderer>();
        tilePos.x = H;
        tilePos.y = W;
    }
    public void SetTileColor(ChessColor color)
    {
        switch(color)
        {
            case ChessColor.BLACK:
                render.color = Color.black;
                break;
                case ChessColor.WHITE:
                render.color = Color.white;
                break;
        }
    }
}
