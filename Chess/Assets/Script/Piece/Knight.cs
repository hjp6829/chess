using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Knight : Piece
{
    private HashSet<Vector2Int> blockFriendlyVector = new HashSet<Vector2Int>();
    protected override void Start()
    {
        base.Start();
    }
    public override List<Vector2Int> MoveLocations(Vector2Int Pos)
    {
        List<Vector2Int> locations = new List<Vector2Int>();
        locations.Add(new Vector2Int(Pos.x + 1, Pos.y + 2));
        locations.Add(new Vector2Int(Pos.x + 2, Pos.y + 1));
        locations.Add(new Vector2Int(Pos.x + 2, Pos.y - 1));
        locations.Add(new Vector2Int(Pos.x + 1, Pos.y - 2));
        locations.Add(new Vector2Int(Pos.x-1, Pos.y - 2));
        locations.Add(new Vector2Int(Pos.x-2, Pos.y - 1));
        locations.Add(new Vector2Int(Pos.x - 2, Pos.y+1));
        locations.Add(new Vector2Int(Pos.x - 1, Pos.y+2));
        return locations;
    }
    public override List<Vector2Int> ExceptionPos()
    {
        List<Vector2Int> temp = MoveLocations(tilePos);
        HashSet<int> removeIDX = new HashSet<int>();
        blockFriendlyVector.Clear();
        ChessDatamanager datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        for (int i = 0; i < temp.Count; i++)
        {
            if (!IsValidPosition(temp[i]))
            {
                removeIDX.Add(i);
                continue;
            }
            ChessColor chessColor = datamanager.GetChessTileData(tilePos).pieceColor;
            ChessColor selectColor = datamanager.GetChessTileData(temp[i]).pieceColor;
            if (chessColor == selectColor)
            {
                blockFriendlyVector.Add(temp[i]);
            }
        }
        return temp.Where((t, i) => !removeIDX.Contains(i)).ToList();
    }
    public override List<Vector2Int> MakeMovePos()
    {
        List<Vector2Int> temp = ExceptionPos();
        return temp.Where((t, i) => !blockFriendlyVector.Contains(t)).ToList();
    }
}
