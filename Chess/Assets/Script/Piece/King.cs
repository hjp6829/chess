using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class King : Piece
{
    private bool canCastling = false;
    private Vector2Int castlingPos;
    private Vector2Int castlingRookPos;

    public Vector2Int CastlingPos { get => castlingPos; }
    public Vector2Int CastlingRookPos { get => castlingRookPos; }
    public bool CanCastling { get => canCastling; }
    protected override void Start()
    {
        base.Start();
    }
    public override List<Vector2Int> MoveLocations(Vector2Int Pos)
    {
        canCastling = false;
        List<Vector2Int> locations = new List<Vector2Int>();
        locations.Add(new Vector2Int(Pos.x + 1, Pos.y + 1));
        locations.Add(new Vector2Int(Pos.x - 1, Pos.y - 1));
        locations.Add(new Vector2Int(Pos.x + 1, Pos.y - 1));
        locations.Add(new Vector2Int(Pos.x - 1, Pos.y + 1));
        locations.Add(new Vector2Int(Pos.x, Pos.y + 1));
        locations.Add(new Vector2Int(Pos.x, Pos.y - 1));
        locations.Add(new Vector2Int(Pos.x + 1, Pos.y));
        locations.Add(new Vector2Int(Pos.x - 1, Pos.y));
        return locations;
    }

    public override List<Vector2Int> ExceptionPos()
    {
        List<Vector2Int> temp = MoveLocations(tilePos);
        HashSet<int> removeIDX = new HashSet<int>();
        ChessDatamanager datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        for (int i = 0; i < temp.Count; i++)
        {
            if (CheckIDX(removeIDX, removeIDX, i, 8))
                continue;
            if (!IsValidPosition(temp[i]))
            {
                removeIDX.Add(i);
                continue;
            }
            ChessColor chessColor = datamanager.GetChessTileData(tilePos).pieceColor;
            ChessColor selectColor = datamanager.GetChessTileData(temp[i]).pieceColor;
            if (chessColor == selectColor)
            {
                removeIDX.Add(i);
            }
        }
        return temp.Where((t, i) => !removeIDX.Contains(i)).ToList();
    }
    public override List<Vector2Int> MakeMovePos()
    {
        ChessDatamanager datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        List<Vector2Int> canMove = ExceptionPos();
        HashSet<Vector2Int> Temp = datamanager.CheckAttackTile(pieceColor);
        canMove = canMove.Where(t => !Temp.Contains(t)).ToList();
        if (isActive || Temp.Contains(tilePos))//king이 움직였거나 공격받는중이라면 공격받는중인거는 캐슬링때문에 체크
            return canMove;
        List<Piece> castlingRooks= datamanager.GetCastlingRooks(pieceColor);
        if(castlingRooks.Count==0)
            return canMove;
        for(int i= castlingRooks.Count-1;i>=0;i--)
        {
            if (!CheckCastling(tilePos, castlingRooks[i].TilePos, datamanager.ChessTileDatas, Temp))
                castlingRooks.RemoveAt(i);
        }
        if (castlingRooks.Count > 0)
        {
            canCastling = true;
            foreach (var piece in castlingRooks)
                canMove.Add(piece.TilePos);
        }
        return canMove;
    }
    private bool CheckCastling(Vector2Int kingPos, Vector2Int rookPos, ChessTileData[,] data,HashSet<Vector2Int> attackedPos)
    {
        int dis = rookPos.y - kingPos.y;
        int checkCount = dis < 0 ? -1 : 1;
        dis=Math.Abs(dis);
        int y = kingPos.y+ checkCount;
        int CASTLING_RANGE = 2;
        for (int i=0;i< dis-1;i++)
        {
            if (data[kingPos.x, y].pieceType != ChessPiece.NONE)
                return false;
            y += checkCount;
        }
        y = kingPos.y + checkCount;
        for (int i = 0; i < CASTLING_RANGE; i++)
        {
            if (attackedPos.Contains(new Vector2Int(kingPos.x, y)))
                return false;
            y += checkCount;
        }
        return true;
    }
    public void MakeCastlingPos(Vector2Int kingPos, Vector2Int rookPos)
    {
        int dis = rookPos.y - kingPos.y;
        int checkCount = dis < 0 ? -1 : 1;
        castlingPos = new Vector2Int(kingPos.x, kingPos.y + checkCount + checkCount);
        castlingRookPos = new Vector2Int(castlingPos.x, castlingPos.y - checkCount);
    }
}
