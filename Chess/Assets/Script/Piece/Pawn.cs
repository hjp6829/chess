using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pawn : Piece
{
    private int forwardDirection;
    protected override void Start()
    {
        base.Start();
    }
    public override List<Vector2Int> MoveLocations(Vector2Int Pos)
    {
        List<Vector2Int> locations = new List<Vector2Int>();
        ChessDatamanager datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        ChessColor pieceColor = datamanager.GetChessTileData(Pos).pieceColor;
        forwardDirection = (pieceColor == ChessColor.WHITE) ? 1 : -1;

        Vector2Int[] directionOffsets = new Vector2Int[] { new Vector2Int(0, 1), new Vector2Int(0, -1) };
        for (int i = 0; i < directionOffsets.Length; i++)
        {
            locations.Add(new Vector2Int(Pos.x + forwardDirection + directionOffsets[i].x, Pos.y + directionOffsets[i].y));
        }
        return locations;
    }
    public override List<Vector2Int> ExceptionPos()
    {
        List<Vector2Int> locations= MoveLocations(tilePos);
        for (int i= locations.Count-1;i>=0;i--)
        {
            if (!IsValidPosition(locations[i]))
                locations.RemoveAt(i);
        }
        return locations;
    }
    public override List<Vector2Int> MakeMovePos()
    {
        List<Vector2Int> locations = ExceptionPos();
        if (!isActive)
        {
            locations.Add(new Vector2Int(tilePos.x + forwardDirection + forwardDirection, tilePos.y));
            locations.Add(new Vector2Int(tilePos.x + forwardDirection, tilePos.y));
        }
        else
            locations.Add(new Vector2Int(tilePos.x + forwardDirection, tilePos.y));
        ChessDatamanager datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        Vector2Int[] directionOffsets = new Vector2Int[] { new Vector2Int(0, 0), new Vector2Int(0, 1), new Vector2Int(0, -1) };
        Vector2Int twoStepsForwardPos = new Vector2Int(tilePos.x + forwardDirection + forwardDirection + directionOffsets[0].x, tilePos.y + directionOffsets[0].y);
        for (int i = 0; i < directionOffsets.Length; i++)
        {
            Vector2Int oneStepForwardPos = new Vector2Int(tilePos.x + forwardDirection + directionOffsets[i].x, tilePos.y + directionOffsets[i].y);
            if (!IsValidPosition(oneStepForwardPos))
            {
                continue;
            }

            ChessColor oneStepForwardPosColor = datamanager.GetChessTileData(oneStepForwardPos).pieceColor;
            if (oneStepForwardPosColor != ChessColor.NONE)
            {
                if (i == 0)
                {
                    locations.Remove(oneStepForwardPos);
                    if(!isActive)
                        locations.Remove(twoStepsForwardPos);
                }
                else if (oneStepForwardPosColor == pieceColor)
                {
                    locations.Remove(oneStepForwardPos);
                }
            }

            if (!isActive)
            {
                ChessColor twoStepsForwardPosColor = datamanager.GetChessTileData(twoStepsForwardPos).pieceColor;
                if (twoStepsForwardPosColor != ChessColor.NONE)
                    locations.Remove(twoStepsForwardPos);
            }
            if (oneStepForwardPosColor == ChessColor.NONE)
            {
                if (i > 0)
                    locations.Remove(oneStepForwardPos);
            }
        }
        #region ������
        //Vector2Int[] directionOffsets = new Vector2Int[] { new Vector2Int(0, 0), new Vector2Int(0, 1), new Vector2Int(0, -1) };
        //Vector2Int twoStepsForwardPos = new Vector2Int(tilePos.x + forwardDirection + forwardDirection + directionOffsets[0].x, tilePos.y + directionOffsets[0].y);
        //for (int i = 0; i < directionOffsets.Length; i++)
        //{
        //    Vector2Int oneStepForwardPos = new Vector2Int(tilePos.x + forwardDirection + directionOffsets[i].x, tilePos.y + directionOffsets[i].y);
        //    if (!IsValidPosition(oneStepForwardPos))
        //    {
        //        continue;
        //    }

        //    ChessColor oneStepForwardPosColor = datamanager.GetChessTileData(oneStepForwardPos).pieceColor;
        //    if (oneStepForwardPosColor != ChessColor.NONE)
        //    {
        //        if (i == 0)
        //        {
        //            locations.Clear();
        //        }
        //        else if (oneStepForwardPosColor == pieceColor)
        //        {
        //            locations.Add(oneStepForwardPos);
        //        }
        //    }
        //    if (!isActive)
        //    {
        //        ChessColor twoStepsForwardPosColor = datamanager.GetChessTileData(twoStepsForwardPos).pieceColor;
        //        if (twoStepsForwardPosColor != ChessColor.NONE)
        //            locations.Remove(twoStepsForwardPos);
        //    }
        //}
        #endregion
        return locations;
    }
    public override void SetNewPos()
    {
        int nextPos = tilePos.x + forwardDirection;
        if (nextPos < 0 || nextPos > 7)
        {
            UImanager uimanager = ServiceLocatorManager.GetService<UImanager>();
            uimanager.GetUI<PawnPromotionUI>(UItype.PAWNPROMOTION).SetPromotionPawn(this);
            uimanager.OpenUI(UItype.PAWNPROMOTION);
        }
    }
}
