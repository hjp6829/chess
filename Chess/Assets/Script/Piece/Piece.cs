using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Piece : MonoBehaviour
{
    [SerializeField]
    protected Vector2Int tilePos;
    protected ChessColor pieceColor;
    protected bool isActive = false;
    protected virtual void Start(){  }

    public bool IsActive { get => isActive; set => isActive = value; }
    public ChessColor PieceColor { get => pieceColor; set => pieceColor = value; }
    public Vector2Int TilePos { get => tilePos; set => tilePos = value; }
    public abstract List<Vector2Int> MoveLocations(Vector2Int Pos);
    protected List<Vector2> FilterLocations(List<Vector2> locations)
    {
        for (int i = locations.Count - 1; i >= 0; i--)
        {
            if (locations[i].x < 0 || locations[i].x > 7)
                locations.RemoveAt(i);
            else if (locations[i].y < 0 || locations[i].y > 7)
                locations.RemoveAt(i);
        }
        return locations;
    }
    public abstract List<Vector2Int> ExceptionPos();
    protected bool CheckIDX(HashSet<int> removeIDX, HashSet<int> checkList, int idx, int dirNum)
    {
        bool isContain = false;
        foreach (var i in checkList)
        {
            if (i % dirNum == idx % dirNum)
            {
                removeIDX.Add(idx);
                isContain = true;
                break;
            }
        }
        return isContain;
    }
    public virtual List<Vector2Int> MakeMovePos()
    {
        return ExceptionPos();
    }
    protected bool IsValidPosition(Vector2Int pos)
    {
        int boardSize = 8;
        return pos.x >= 0 && pos.x < boardSize && pos.y >= 0 && pos.y < boardSize;
    }
    public virtual void SetNewPos() { }
}
