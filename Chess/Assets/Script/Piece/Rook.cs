using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Rook : Piece
{
    private HashSet<Vector2Int> blockFriendlyVector = new HashSet<Vector2Int>();
    protected override void Start()
    {
        base.Start();
    }
    public override List<Vector2Int> MoveLocations(Vector2Int Pos)
    {
        List<Vector2Int> locations = new List<Vector2Int>();
        for (int i=1;i<4;i++)
        {
            locations.Add(new Vector2Int(Pos.x, Pos.y + i));
            locations.Add(new Vector2Int(Pos.x+i, Pos.y));
            locations.Add(new Vector2Int(Pos.x, Pos.y-i));
            locations.Add(new Vector2Int(Pos.x-i, Pos.y));
        }
        return locations;
    }

    public override List<Vector2Int> ExceptionPos()
    {
        List<Vector2Int> temp = MoveLocations(tilePos);
        HashSet<int> removeIDX = new HashSet<int>();
        HashSet<int> blockEnemyIDX = new HashSet<int>();
        HashSet<int> blockFriendlyIDX = new HashSet<int>();
        blockFriendlyVector.Clear();
        ChessDatamanager datamanager = ServiceLocatorManager.GetService<ChessDatamanager>();
        for (int i = 0; i < temp.Count; i++)
        {
            if (CheckIDX(removeIDX, blockFriendlyIDX, i, 4))
                continue;
            if (CheckIDX(removeIDX, blockEnemyIDX, i, 4))
                continue;
            if (!IsValidPosition(temp[i]))
            {
                removeIDX.Add(i);
                continue;
            }
            ChessColor chessColor = datamanager.GetChessTileData(tilePos).pieceColor;
            ChessColor selectColor = datamanager.GetChessTileData(temp[i]).pieceColor;
            if (chessColor == selectColor)
            {
                blockFriendlyVector.Add(temp[i]);
                blockFriendlyIDX.Add(i);
            }
            else if (selectColor != ChessColor.NONE && selectColor != chessColor)
            {
                blockEnemyIDX.Add(i);
            }
        }
        return temp.Where((t, i) => !removeIDX.Contains(i)).ToList();
    }
    public override List<Vector2Int> MakeMovePos()
    {
        List<Vector2Int> temp = ExceptionPos();
        return temp.Where((t, i) => !blockFriendlyVector.Contains(t)).ToList();
    }
}
